<?php

namespace App\Http\Controllers;

use App\Models\ReportDetail;
use Illuminate\Http\Request;

class ReportDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(ReportDetail $reportDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ReportDetail $reportDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ReportDetail $reportDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(ReportDetail $reportDetail)
    {
        //
    }
}
