<?php

namespace App\Http\Controllers;

use App\Models\HistoryDetail;
use Illuminate\Http\Request;

class HistoryDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(HistoryDetail $historyDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(HistoryDetail $historyDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, HistoryDetail $historyDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(HistoryDetail $historyDetail)
    {
        //
    }
}
